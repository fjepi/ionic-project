angular.module('app')

.directive('hangmanStep', function() {
	
	var linkFunction = function(scope, element, attributes){
			console.log('scope');
	}

	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'templates/hangman_step.html',
		link: linkFunction,
		scope: {
			index: '@'
		}

	}

});

angular.module('app')

.directive('hangmanData', function() {

	var linkFunction = function(scope, element, attributes){


		scope.$watch('word', function(newValue, oldValue){
			console.log('watch : ', scope.word);

			// on créer un tableau letters propre au code html qui va automatiquement être accessible dans la vue (html)
			scope.letters = [];

			if(scope.word && scope.word != undefined) {
				var first = scope.word.charAt(0);
				for(var i = 0; i < scope.word.length; i++){
					if(scope.word.charAt(i) == first){
						scope.letters.push(first);
					} else {
						scope.letters.push('_');
					}
				};
			}
		})


		// on range la premiere lettre du mot dans le tableau

		scope.$watchCollection('found', function(newValue, oldValue){

			if(scope.word && scope.word != undefined) {

				for(var i = 0; i < scope.word.length; i++){

					var l = scope.word.charAt(i);
					if(_.indexOf(scope.found, l) != -1){
						scope.letters[i] = l;
					}
				};
				if(_.indexOf(scope.letters, '_') == -1){
					//console.log('finish');
					scope.finish = true;
				}
			}

			console.log(scope.found);
		});
	}

	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'templates/hangman_data.html',
		link: linkFunction,
		scope: {
			word: '=',
			found: '=',
			finish: '='
		}
	}
	
});