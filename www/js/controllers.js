angular.module('app')

.controller('PopupCtrl',function($scope, $ionicPopup, $timeout, $ionicModal, MyStorage) {

  MyStorage.set('nb', 42);
  MyStorage.setObject('obj', $scope.game);


  $scope.game = {
    currentStep: 0,
    found: [], // tableau d'objet
    nbStep: 10,
    finish: false, // Camel Case in html replace by dash
    currentPlayer: 'user',
    symbol: 'x',
    grid: { row1:['_','_','_'], row2: ['_','_','_'], row3: ['_','_','_']}
  }

  $scope.$watch('game.finish', function(newValue, oldValue){
      console.log($scope.game.finish);
      if($scope.game.finish == true){
        $scope.$broadcast('game::end');
      }
  });

  $scope.random = function(tab){
    var item = tab[Math.floor(Math.random()*tab.length)];

    return item;
  }

   $ionicModal.fromTemplateUrl('templates/Modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.OpenModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
   $scope.$on('modal.shown', function() { 
      $ionicPopup.confirm({
        title: 'Choose your symbol',
        cancelText: 'x',
        okText: 'o'
       }).then(function(res) {
        if(res) {
          $scope.game.symbol ='o';
        } else {
          $scope.game.symbol = 'x';
        }
        $scope.$broadcast('game::next');
       });
     })

  $scope.nextStep = function(){
    if( $scope.game.currentStep != $scope.game.nbStep) {
        $scope.game.currentStep++;
        if($scope.game.currentStep == 10 ){
          $scope.$broadcast('game::end');
        }
    }
  }

  $scope.$on('game::next', function() {
    $scope.interval = setInterval(function() {
      $scope.nextStep();
      // force l'update de la variable qui a changé dans le setInterval
      $scope.$apply();
    }, 3000);
  });

  $scope.$on('game::end', function(){
    clearInterval($scope.interval);
    var my_title, my_template;
    if($scope.game.finish == true){
      my_title = 'YOU WIN BIBIBIATCH';
      my_template = 'so crazy right now!';
    } else {
        my_title = 'You Loose Naaaaab!';
        my_template = 'this is what you are!';
    }
    $ionicPopup.alert({
        title: my_title,
        template: my_template
    }).then(function (){

    })
      
  })

  $scope.check = function(key, value){
    console.log($scope.game.symbol);
    console.log($scope.game.grid[key]);
    if($scope.game.grid[key] == '_') {
      $scope.game.grid[key] = $scope.game.symbol;
    }


  }

});

