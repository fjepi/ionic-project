// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic','ngCordova'])

.run(function($ionicPlatform, DatabaseService) {
  $ionicPlatform.ready(function() {

    
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
    //faire 
    .state('tabs', {
      url: '/tabs',
      templateUrl: 'templates/tabs.html',
      abstract: true
    })

    .state('tabs.scoring', {
      url: '/scoring',
      views: {
      'tab-scoring': {
        templateUrl: 'templates/Scoring.html'
      }
    }
    })

    .state('tabs.game', {
      url: '/game',
      views: {
      'tab-game': {
        templateUrl: 'templates/Game.html'
      }
    }
    })
    
    .state('tabs.settings', {
      url: '/settings',
      views: {
      'tab-settings': {
        templateUrl: 'templates/Settings.html'
      }
    }
    })

    .state('tabs.player', {
        url: '/player',
        views: {
        'tab-game': {
          templateUrl: 'templates/Play.html',
          controller: 'PopupCtrl'
        }
      }
      })
    ;

  // if none of the above states are matched, use this as the fallback
  
  $urlRouterProvider.otherwise('/tabs/player');
  

});

